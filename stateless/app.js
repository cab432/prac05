var express = require('express');
var app = express();

// Need this to access the body parameters from the POST request because it is deprecated in core Express.
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));

// Array to hold all the messages in the conversation.
var conversation = [];

app.get('/', function (req, res) {
  res.sendFile('views/index.html', { root: __dirname });
});

app.post('/messages', function (req, res) {
  // Check to make sure the request has sufficient information.
  if (req.body == null
      || req.body.user == null || req.body.user == ""
      || req.body.text == null || req.body.text == "") {
    res.json({ error: "must provide non-empty `user` and `text` parameters" });
    return;
  }

  var message = {
    ip: req.ip,
    timestamp: new Date(),
    user: req.body.user,
    text: req.body.text
  };
  conversation.push(message);
  res.json({ ok: true }); // The client can check this to make sure their message was received.
});

app.get('/messages', function (req, res) {
  if (req.query.lastSeen) {
    // We need to convert our `lastSeen` string into a `Date` object so we can compare it with the message timestamps.
    var lastSeenDate = new Date(req.query.lastSeen);

    // From the most recent message (the one pushed last), go backwards until we run out of messages or we have
    // already seen the message.
    var i;
    for (i = conversation.length - 1; i >= 0 && lastSeenDate < conversation[i].timestamp; i--) ;

    // Slice the array. `slice()` does not mutate the array, but returns a new array with the sliced elements.
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/slice
    res.json({ ok: true, messages: conversation.slice(i + 1) });

  } else {
    // Otherwise we can send *all* the messages.
    res.json({ ok: true, messages: conversation });
  }

});

var server = app.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('Stateless messaging app listening at http://%s:%s', host, port);
});
