# Stateless Messaging

This is a solution to part of Practical 5. It does:
   
  - filtering of messages by the `lastSeen` query parameter
  - periodic polling which appends new messages to the list
 
It does not do persistent storing of messages with a database.

Some things to note:

  - it doesn't follow the exact same HTML layout as presented in the practical material (in particular, forms are removed)
  - if an error occurs when the server handles a request, it'll respond with `{ "error": "error message..." }`
  - if an error handles a request succesfully, it'll respond with `{ "ok": true, ...payload data... }`
  
Don't feel like you absolutely *have* to do these, but I think they are good.

# Install

In this directory, install the dependencies by running: 

    npm install

This will install all the modules listed under `dependencies` in the file `package.json` to the directory `node_modules`.
If you version control your code, you can ignore the `node_modules` folder.

# Run

In this directory, start the server by running:

    npm start
    
This will execute the command `node ./app.js`, which is in the `scripts` section of the `package.json`. 